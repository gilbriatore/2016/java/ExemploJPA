package br.edu.up.core;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.edu.up.dominio.Endereco;
import br.edu.up.dominio.Pessoa;

public class Programa {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("ExemploJPA");
		EntityManager em = emf.createEntityManager();
		TypedQuery<Pessoa> query = em.createQuery("select p from Pessoa p", Pessoa.class);
		List<Pessoa> lista = query.getResultList();
		for (Pessoa pessoa : lista) {
			System.out.println("Nome: " + pessoa.getNome());
			if (pessoa.getEnderecos().size() > 0){
				Endereco endereco = pessoa.getEnderecos().get(0);
			   System.out.println("Endere�o: " + endereco.getRua());
			}
		}

	}

}
